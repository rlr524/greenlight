package main

import (
	"fmt"
	"net/http"
)

// createMovieHandler handles the "POST /v1/movies" endpoint
func (app *Application) createMovieHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "create a new movie")
}

// showMovieHandler handles the "GET /v1/movies" endpoint
func (app *Application) showMovieHandler(w http.ResponseWriter, r *http.Request) {
	// If the parameter passed to readIDParam couldn't be converted, or is less than 1, we know the ID is invalid,
	// so we use the http.NotFound() function to return a 404 Not Found response.
	id, err := app.readIDParam(r)
	if err != nil {
		http.NotFound(w, r)
		return
	}

	fmt.Fprintf(w, "show the details of movie %d\n", id)
}
