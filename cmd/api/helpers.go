package main

import (
	json2 "encoding/json"
	"errors"
	"github.com/julienschmidt/httprouter"
	"net/http"
	"strconv"
)

func (app *Application) writeJSON(w http.ResponseWriter, status int, data any, headers http.Header) error {
	json, err := json2.Marshal(data)
	if err != nil {
		return err
	}

	json = append(json, '\n')

	for key, value := range headers {
		w.Header()[key] = value
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write(json)

	return nil
}

// readIDParam retrieves the "id" URL parameter from the current request context, then converts it
// to an integer and returns it. If the operation isn't successful, 0 is returned with an error.
func (app *Application) readIDParam(r *http.Request) (int64, error) {
	// When httprouter is parsing a request, any interpolated URL parameters will be stored in the request
	// context. Use the ParamsFromContext function to retrieve a slice containing these parameter names
	// and values.
	params := httprouter.ParamsFromContext(r.Context())

	// We can then use the ByName() method to get the value of the "id" parameter from the slice. In our
	// project all movies will have a unique positive integer ID, but the value returned by ByName() is
	// always a string. So we try to convert it to a base 10 integer (with a bit size of 64).
	id, err := strconv.ParseInt(params.ByName("id"), 10, 64)
	if err != nil || id < 1 {
		return 0, errors.New("invalid id parameter")
	}

	return id, nil
}
